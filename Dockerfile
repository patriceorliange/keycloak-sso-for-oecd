FROM maven:3.8.1-jdk-11-slim AS build
WORKDIR /app

#---- download and cache dependencies 
COPY pom.xml ./
RUN mvn -f /app/pom.xml dependency:go-offline

#---- package module to jar
COPY src ./src
RUN mvn -f /app/pom.xml package -DskipTests

FROM jboss/keycloak:12.0.4 AS runtime
COPY --from=build /app/target/saml-group-mapper.jar /opt/jboss/keycloak/standalone/deployments/
COPY ./themes /opt/jboss/keycloak/themes
