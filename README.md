# Keycloak

The goal of this repo is to push a custom keycloak image with embendded themes but also to be the starting point for authentication documentation


## Keycloak Introduction

The awesome open-source Identity and Access Management solution develop by RedHat.
Keycloak support those very nice features you are looking for:
- Single-Sign On
- LDAP and Active Directory
- Standard Protocols
- Social Login
- Clustering
- Custom Themes
- Centralized Management
- Identity Brokering
- Extensible
- Adapters
- High Performance
- Password Policies

More about Keycloak: http://www.keycloak.org/


## Auth Architecture

![archi](./docs/auth_service.jpg)

The new architecture enhanced with authentication service is mono access manager but multi ID providers.

* Access Manager server implements access workflows and render HTML pages for signin, signout, registration (and all related actions around login). It allows to forward login access to external ID providers.
* ID providers create, maintain, and manage identity information (LDAP and Active Directory, Social Login, OpenId Connect, local provider)

Keycloak acts as **the** access manager, but can also be used as a local ID provider. First role is mandatory, latter optional.



## Keycloak in a nutshell


To setup Keycloak we need to create a `realm` for each `tenant`.

A `realm` exports public keys to decode JWT token.

If keycloak acts as an ID provider, users will be created within a realm associated with roles.

A `realm` may route to external ID providers (AD, LDAP, socials networks, OpenID, ...)


## Auth workflow 

![archi](./docs/auth_workflow.png)

1. At loading time, App must received all required information to connect to keycloak server. It could come directly from server's params or extracted from the `config` server within `tenants.json` file (see below)
2. If user requests a protected route, and user is not yet authenticated, App will redirect to a keycloak login form to get a keycloak JWT. Keycloak can act as an ID provider but also as an access management hub connected with many external existing  ID providers
3. Backend requests are associated with a Bearer token (keycloak token) 
4. Backends can verify token `online` or `offline`. `online` verification implies to request keycloak server on each App request, whereas `offline` one implies to verify an decode a JWT locally, but needs to install keycloak public keys for the targeted realm (they is an option to request keycloak pulic key). If verification succeeded, back end will get user information, including roles and can perform local authentification and authorization. 


## Usage


IN local mode, run keycloak service with `KEYCLOAK_USER` and `KEYCLOAK_PASSWORD` vars set for your admin credentials. 

```
$ docker run -d --name=keycloak --restart=always -p 8080:8080 -e KEYCLOAK_USER=admin_user_name -e KEYCLOAK_PASSWORD=admin_password -e DB_VENDOR=H2 siscc/dotstatsuite-keycloak
```


## Keycloak configuration

Follow this steps:

  * Once the keycloak container is started, launch you browser on <server_url>/auth/admin then log in using the admin `credentials`
  
  * Create a [realm](https://www.keycloak.org/docs/6.0/server_admin/#_create-realm) and make sure to `enable` it.
  
  * Create a [client](https://www.keycloak.org/docs/6.0/server_admin/#_clients) and make sure it is `public`. Make sure to set at least `Valid Redirect URIs` to `http://localhost:7000/*`, `Web Origins` to `http://localhost:7000` and `Access Type` to `public`
  
  In the client creation form, with the field `login theme` you can pick a customised theme you previously added.

* last step is to link the tenant with the Keycloak setup. Edit `/configs/tenants.json` file from the [config server project](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config) and set a `keycloak` field in a `tenant object` containing the `name` of the `realm` and the `id` of the `client`.

For example with a `oecd` tenant it can look like this:

```
  "oecd": {
    "id": "oecd",
    "name": "OECD",
    "keycloak": {
      "realm": "OECD",
      "clientId": "app"
    }
```




## Theme

Keycloak allows you to define `custom UI`.

You can add a specific theme for login, account management, emails and more.

Since creating an entire theme can be quite fastidious, it is recommended to extend/modify an existing theme instead of creating a new one.

For furthermore information about keycloak themes please see the [documentation](https://www.keycloak.org/docs/latest/server_development/index.html#_themes).

Once you have your specific theme just drag it into the `/themes` of this repo.




### Validating access tokens

[webapp](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-webapp) uses the lib [keycloak-verify](https://www.npmjs.com/package/keycloak-verify) to verify keycloak's tokens and get UserInfo


> If you are using Docker you can use PROXY_ADDRESS_FORWARDING environment var from the original Keycloak container to set this attribute.